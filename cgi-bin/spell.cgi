#!/opt/csw/bin/perl -w
#
#    Copyright (C) 2009~2014 SHIE, Li-Yi (lyshie) <lyshie@mx.nthu.edu.tw>
#
#    https://github.com/lyshie
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation,  either version 3 of the License,  or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not,  see <http://www.gnu.org/licenses/>.
#

use strict;
use warnings;

use FindBin qw($Bin);
use CGI qw(:standard);
use CGI::Lite;
use PHP::Session;
use Digest::MD5 qw(md5_hex);
use URI;

my $WIKI_URL     = 'http://wiki.net.nthu.edu.tw/';
my $SESSION_NAME = 'DokuWiki';
my $SESSION_PATH = '/tmp';

my $PHP          = '/opt/csw/php5/bin/php';
my $DOKUWIKI     = '/usr/local/dokuwiki-2008-05-05/';
my $DOKU_TO_LIST = '/usr/local/doku2slist';
my $DOKUWIKI_URL = 'http://net.nthu.edu.tw/2009/';
my $REMOTE_USER  = '';

my $PHRASES_PATH = "$Bin/phrases";

# lyshie_20090210: get 'DW##############' key
my $uri = new URI($WIKI_URL);
my $rel = $uri->path || '/';
my $key = 'DW' . md5_hex($rel);

# lyshie_20090210: get cookies
my $cgi     = new CGI::Lite;
my $cookies = $cgi->parse_cookies();
my $session;
my $auth;
if ( defined( $cookies->{$SESSION_NAME} ) ) {
    $session = PHP::Session->new( $cookies->{$SESSION_NAME},
        { 'save_path' => $SESSION_PATH } );

    $auth = $session->get($key)->{auth};
    $REMOTE_USER = $auth->{'user'} || '';
}

unless ($REMOTE_USER) {
    print header( -charset => 'utf-8' );
    print '您尚未登入！';
    exit();
}

# lyshie_20090210: cgi start

my $id = param('id') || '';
$id =~ s/[^a-zA-Z0-9:_]//g;

my $wiki    = `$PHP $DOKU_TO_LIST/getwiki_tome.php $id`;
my $wikiraw = `$PHP $DOKU_TO_LIST/getwikiraw_tome.php $id`;

sub ngram_load {
    my ($size) = @_;
    my $table = '';

    open( FH, "$PHRASES_PATH/tab_$size" );
    local $/;
    $table = <FH>;
    close(FH);

    $table =~ s/\n+/|/g;

    return "$table";
}

my $reg = ngram_load(0) . ngram_load(4) . ngram_load(3) . ngram_load(2);
$reg =~ s/\|$//g;
$wikiraw =~ s/([\x00-\x7f]+|$reg)/<_begin_>$1<_end_>/g;
$wikiraw =~ s/<_begin_>/<\/u><font style="color: gray;">/g;
$wikiraw =~ s/<_end_>/<\/font><u>/g;

# lyshie_20090210: dump the result
print header( -charset => 'utf-8' );
print qq{<pre style="font-size: 10pt; color: brown;"><u>};
print $wikiraw;
print qq{</u></pre>};
