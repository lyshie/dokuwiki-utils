#!/usr/local/bin/perl -w
#
#    Copyright (C) 2009~2014 SHIE, Li-Yi (lyshie) <lyshie@mx.nthu.edu.tw>
#
#    https://github.com/lyshie
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation,  either version 3 of the License,  or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not,  see <http://www.gnu.org/licenses/>.
#

use strict;
use warnings;

my $HOSTNAME = `/bin/hostname`;
chomp($HOSTNAME);

die("ERROR: The hostname should be 'net'!") if ( $HOSTNAME ne 'net' );

my $DOKU_BASE = '/usr/local/dokuwiki2009';
my $CHOWN     = '/bin/chown';
my $OWNER     = 'nobody:mail';

chmod( 0555, $DOKU_BASE ) if ( -d "$DOKU_BASE" );

#`$CHOWN -R $OWNER $DOKU_BASE/data/attic` if (-d "$DOKU_BASE/data/attic");
#`$CHOWN -R $OWNER $DOKU_BASE/data/cache` if (-d "$DOKU_BASE/data/cache");
#`$CHOWN -R $OWNER $DOKU_BASE/data/index` if (-d "$DOKU_BASE/data/index");
#`$CHOWN -R $OWNER $DOKU_BASE/data/meta` if (-d "$DOKU_BASE/data/meta");
#`$CHOWN -R $OWNER $DOKU_BASE/data/locks` if (-d "$DOKU_BASE/data/locks");
#`$CHOWN -R $OWNER $DOKU_BASE/data/tmp` if (-d "$DOKU_BASE/data/tmp");
