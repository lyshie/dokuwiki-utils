#!/bin/sh
#
#    Copyright (C) 2009~2014 SHIE, Li-Yi (lyshie) <lyshie@mx.nthu.edu.tw>
#
#    https://github.com/lyshie
#	
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation,  either version 3 of the License,  or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful, 
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not,  see <http://www.gnu.org/licenses/>.
#

HOSTNAME=x`/bin/hostname`

if [ ${HOSTNAME} = x'net' ]; then
	PHP=/usr/local/bin/php
	DOKUWIKI=/usr/local/dokuwiki2009
	/bin/su - nobody -c "${PHP} ${DOKUWIKI}/bin/indexer.php"
elif [ ${HOSTNAME} = x'wiki' ]; then
	PHP=/opt/csw/php5/bin/php
	DOKUWIKI=/usr/local/dokuwiki-2008-05-05
	/bin/su - wiki -c "${PHP} ${DOKUWIKI}/bin/indexer.php -c"
else
	echo "ERROR!"
	exit
fi
