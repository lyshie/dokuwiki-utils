#!/opt/csw/bin/perl -w
#
#    Copyright (C) 2009~2014 SHIE, Li-Yi (lyshie) <lyshie@mx.nthu.edu.tw>
#
#    https://github.com/lyshie
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation,  either version 3 of the License,  or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not,  see <http://www.gnu.org/licenses/>.
#

use strict;
use warnings;

use CGI qw(:standard);
use CGI::Lite;
use MIME::Lite;
use MIME::Words qw(:all);
use PHP::Session;
use Digest::MD5 qw(md5_hex);
use URI;

my $WIKI_URL     = 'http://wiki.net.nthu.edu.tw/';
my $SESSION_NAME = 'DokuWiki';
my $SESSION_PATH = '/tmp';

my $PHP          = '/opt/csw/php5/bin/php';
my $DOKUWIKI     = '/usr/local/dokuwiki-2008-05-05/';
my $DOKU_TO_LIST = '/usr/local/doku2slist';
my $DOKUWIKI_URL = 'http://net.nthu.edu.tw/2009/';
my $REMOTE_USER  = '';
my $EMAIL        = '';
my $SUBJECT      = '';

# lyshie_20090210: get 'DW##############' key
my $uri = new URI($WIKI_URL);
my $rel = $uri->path || '/';
my $key = 'DW' . md5_hex($rel);

# lyshie_20090210: get cookies
my $cgi     = new CGI::Lite;
my $cookies = $cgi->parse_cookies();
my $session;
my $auth;
if ( defined( $cookies->{$SESSION_NAME} ) ) {
    $session = PHP::Session->new( $cookies->{$SESSION_NAME},
        { 'save_path' => $SESSION_PATH } );

    $auth = $session->get($key)->{auth};
    $REMOTE_USER = $auth->{'user'} || '';
}

unless ($REMOTE_USER) {
    print header( -charset => 'utf-8' );
    print '您尚未登入！';
    exit();
}

# lyshie_20090210: cgi start

my $id = param('id') || '';
$id =~ s/[^a-zA-Z0-9:_]//g;

if ( $id !~ /^mailing:.+$/ ) {
    print header( -charset => 'utf-8' );
    print "[[$id]] 不在 [[mailing:*]] 無法轉成電子報！";
    print exit();
}

# lyshie_20090224: get user's email address

if ( -f "${DOKUWIKI}conf/users.auth.php" ) {
    open( FH, "${DOKUWIKI}conf/users.auth.php" );
    while (<FH>) {
        chomp($_);
        my @tokens = split( /:/, $_ );
        if ( defined( $tokens[0] ) && ( $tokens[0] eq $REMOTE_USER ) ) {
            $EMAIL = $tokens[3] || '';
            last;
        }
    }
    close(FH);
}

my $wiki    = `$PHP $DOKU_TO_LIST/getwiki.php $id`;
my $wikiraw = `$PHP $DOKU_TO_LIST/getwikiraw.php $id`;

my $old  = "$DOKU_TO_LIST/";
my $old2 = $DOKUWIKI;
my $new  = $DOKUWIKI_URL;

$wiki =~ s/\Q$old\E/$new/g;
$wiki =~ s/\Q$old2\E/$new/g;
$wiki =~ s/href="\/(.*?)"/href="$new$1"/g;
$wiki =~ s/src="\/(.*?)"/src="$new$1"/g;
$wiki =~ s/href="\.\/(.*?)"/href="$new$1"/g;

$wikiraw =~ s/[\s\S]*?\/\*\s+(mailing:.*)\s+\*\///;

$SUBJECT = $1 || "無標題";
if ( $1 =~ /^mailing:.*/ ) {
    $SUBJECT =~ s/^mailing://g;
    $SUBJECT =~ s/^\s+//g;
    $SUBJECT =~ s/\s+$//g;
}
unless ($SUBJECT) {
    $SUBJECT = "無標題";
}

#$wikiraw =~ s/\[\[.*?\]\]//g;
$wikiraw =~ s/{{.*?}}//g;

$wikiraw = qq{
以下文字為本期電子報部分內容，
欲瀏覽全文請參閱 ${DOKUWIKI_URL}$id
} . $wikiraw . qq{

-- 
如欲取消訂閱請至 http://list.net.nthu.edu.tw/
};

my $msg = MIME::Lite->new(
    From     => 'netsys@cc.nthu.edu.tw',
    To       => 'netsys@list.net.nthu.edu.tw',
    Bcc      => "${EMAIL}",
    Subject  => encode_mimeword( "${SUBJECT}", "B", "UTF-8" ),
    Type     => 'multipart/alternative',
    Encoding => '7bit',
);
$msg->attr( 'content-type.charset' => 'UTF-8' );

my $htmlpart = MIME::Lite->new(
    Type     => 'text/html',
    Encoding => 'base64',
    Data     => $wiki,
);
$htmlpart->attr( 'content-type.charset' => 'UTF-8' );

my $textpart = MIME::Lite->new(
    Type     => 'TEXT',
    Encoding => 'base64',
    Data     => $wikiraw,
);
$textpart->attr( 'content-type.charset' => 'UTF-8' );

$msg->attach($textpart);
$msg->attach($htmlpart);

$msg->send( 'smtp', 'smtp.net.nthu.edu.tw' );

# lyshie_20090210: dump the result
print header( -charset => 'utf-8' );
print "電子報標題：[$SUBJECT]<br />\n";
print "寄給：[$EMAIL]<br />\n";
print $wiki;
