<?php
#
#    Copyright (C) 2009~2014 SHIE, Li-Yi (lyshie) <lyshie@mx.nthu.edu.tw>
#
#    https://github.com/lyshie
#	
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation,  either version 3 of the License,  or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful, 
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not,  see <http://www.gnu.org/licenses/>.
#

$PHP           = '/opt/csw/php5/bin/php';
$DOKUWIKI      = '/usr/local/dokuwiki-2008-05-05/';
$DOKU_URL      = 'http://net.nthu.edu.tw/2009';
$SMARTLIST     = 'http://list.net.nthu.edu.tw/slist';
$SUBSCRIBE     = "${SMARTLIST}/cgi-bin/subscribe.cgi?lists=netsys";
#$SUBSCRIBE     = "${SMARTLIST}/subscribe.html";
$UNSUBSCRIBE   = "${SMARTLIST}/cgi-bin/unsubscribe.cgi?lists=netsys";
#$UNSUBSCRIBE   = "${SMARTLIST}/subscribe.html";

if(!defined('DOKU_INC')) define('DOKU_INC', $DOKUWIKI);

require_once(DOKU_INC.'inc/init.php');
require_once(DOKU_INC.'inc/common.php');
require_once(DOKU_INC.'inc/pageutils.php');

$DOKU_INC = DOKU_INC;
$TEMPLATE = $conf['template'];

$CSS = shell_exec(escapeshellcmd("${PHP} ${DOKU_INC}lib/exe/css.php \"s=all&amp;t=${TEMPLATE}\""));

echo <<<HEAD
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="zh-tw"
 lang="zh-tw" dir="ltr">
<head>
  <title>From DokuWiki</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <link rel="stylesheet" media="all" type="text/css" href="${DOKU_INC}lib/exe/css.php?s=all&amp;t=${TEMPLATE}" />
  <link rel="stylesheet" media="screen" type="text/css" href="${DOKU_INC}lib/exe/css.php?t=${TEMPLATE}" />
  <style type="text/css">
  ${CSS}
  </style>
</head>
<body>

<div class="dokuwiki" style="margin: auto; width: 640px; border: 1px solid;">
  <div class="stylehead">
	<div class="header">
	  <div class="pagename">
		[[<a href="${DOKU_URL}/">網路系統組</a>]]<a href="http://net.nthu.edu.tw/">http://net.nthu.edu.tw/</a>
	  </div>
	  <div class="logo">
	  </div>
	  <div class="clearer"></div>
	</div>
  </div>
  <div class="stylefoot">
	<div class="bar" id="bar__bottom1">
	  <div class="bar-left" id="bar__bottomleft1">
	  </div>
	  <div class="bar-right" id="bar__bottomright1">
		若無法瀏覽全部圖文，<a href="${DOKU_URL}/$argv[1]">請按這裡</a>
	  </div>
	  <div class="clearer"></div>
	</div>
  </div>
  <div class="page">
HEAD;

if (file_exists(wikiFN($argv[1]))) {
	echo p_wiki_xhtml($argv[1]);
}

echo <<<FOOT

  </div>
  <div class="stylefoot">
	<div class="bar" id="bar__bottom2">
	  <div class="bar-left" id="bar__bottomleft2">
	  </div>
	  <div class="bar-right" id="bar__bottomright2">
		&nbsp;&nbsp;
	  </div>
	  <div class="clearer"></div>
	</div>
	發行單位：國立清華大學 計算機與通訊中心 網路系統組
  </div>
</div>
</body>
</html>
FOOT;
