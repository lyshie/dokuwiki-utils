#!/opt/csw/bin/perl -w
#
#    Copyright (C) 2009~2014 SHIE, Li-Yi (lyshie) <lyshie@mx.nthu.edu.tw>
#
#    https://github.com/lyshie
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation,  either version 3 of the License,  or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not,  see <http://www.gnu.org/licenses/>.
#

use strict;
use warnings;

use MIME::Lite;

my $URLWATCH = '/usr/local/bin/urlwatch';
my $ATOMIC   = '/usr/local/dokuwiki-2008-05-05/bin/atomic.sh';

sub main {
    die("ERROR: $URLWATCH can't be executed!\n") unless ( -x $URLWATCH );

    my $now_str = localtime();
    my $result  = `$URLWATCH`;

    if ($result) {
        my $msg = MIME::Lite->new(
            From     => 'urlwatch@wiki.net.nthu.edu.tw',
            To       => 'sysop@net.nthu.edu.tw',
            Subject  => "[urlwatch] $now_str",
            Type     => 'TEXT',
            Encoding => 'base64',
            Data     => $result,
        );

        $msg->attr( 'content-type.charset' => 'UTF-8' );
        $msg->send( 'smtp', 'smtp.net.nthu.edu.tw' );

        `$ATOMIC`;
    }
}

main();
