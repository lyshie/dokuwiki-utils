#!/opt/csw/bin/perl -w
#
#    Copyright (C) 2009~2014 SHIE, Li-Yi (lyshie) <lyshie@mx.nthu.edu.tw>
#
#    https://github.com/lyshie
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation,  either version 3 of the License,  or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not,  see <http://www.gnu.org/licenses/>.
#

use strict;
use warnings;

use CGI qw(:standard);
use CGI::Lite;
use MIME::Lite;
use MIME::Words qw(:all);
use PHP::Session;
use Digest::MD5 qw(md5_hex);
use URI;

my $WIKI_URL     = 'http://wiki.net.nthu.edu.tw/';
my $SESSION_NAME = 'DokuWiki';
my $SESSION_PATH = '/tmp';

my $DOKUWIKI    = '/usr/local/dokuwiki-2008-05-05';
my $REMOTE_USER = '';

# lyshie_20090210: get 'DW##############' key
my $uri = new URI($WIKI_URL);
my $rel = $uri->path || '/';
my $key = 'DW' . md5_hex($rel);

# lyshie_20090210: get cookies
my $cgi     = new CGI::Lite;
my $cookies = $cgi->parse_cookies();
my $session;
my $auth;
if ( defined( $cookies->{$SESSION_NAME} ) ) {
    $session = PHP::Session->new( $cookies->{$SESSION_NAME},
        { 'save_path' => $SESSION_PATH } );

    $auth = $session->get($key)->{auth};
    $REMOTE_USER = $auth->{'user'} || '';
}

unless ($REMOTE_USER) {
    print header( -charset => 'utf-8' );
    print '您尚未登入！';
    exit();
}

my $cmd    = "$DOKUWIKI/bin/atomic.sh";
my $result = '';
if ( -x $cmd ) {
    $result = `$cmd`;
}
else {
    $result = "找不到 rsync 程式，請通知管理者！";
}

my $tmp = $result;
$result = '';

foreach ( split( /[\n\r]/, $tmp ) ) {
    if ( $_ =~ /skipping/i ) {
        $result .= "<font color=\"gray\">$_</font>\n";
    }
    else {
        $result .= "<font color=\"blue\">$_</font>\n";
    }
}

print header( -charset => 'utf-8' );
print '<pre style="font-size: 10pt">';
print $result;
print '</pre>';
